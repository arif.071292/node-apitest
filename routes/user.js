var express = require('express');
var router = express.Router();
var User 	= require('../models/user');


// Create User
router.post('/', function (req,res,next) {
	sid = req.body.sid ;
	name = req.body.name ;
	dept = req.body.dept ;

	if (!sid || sid.length == 0 || !name || name.length == 0 || !dept || dept.length == 0){
		res.status(400).json({
			message : "Student Id, Name and Dept Can't be Empty"
		});
	} else{
		user = new User() ;
		user.sid = sid ;
		user.name = name ;
		user.dept = dept ;
		user.created = new Date() ;

		user.save(function(error){
			if(error){
				res.status(500).json({
					message : "Database Error"
				});
			} else{
				res.json({
					message : "New User Successfully Created!"
				});
			}
		});
	}
});


// Get ALL User
router.get('/', function(req,res,next){
	User.find({},function(error,user){
		if (error){
			res.status(500).json({
				message : "Database Error"
			});
		} else{
			res.json(user);
		}
	})
});


// Get specific User
router.get('/:sid', function(req,res,next){
	sid = req.params.sid ;
	User.findOne({sid:sid},function(error,user){
		if (error){
			res.status(500).json({
				message : "Database Error"
			});
		} else{
			if(user)
				res.json(user);
			else{
				res.status(400).json({
					message : "User Doesn't Exists!"
				});
			}
		}
	})
});


// Update User
router.put('/:sid', function(req,res,next){
	sid = req.params.sid ;

	User.findOne({sid : sid}, function(error,user){
		if (error){
			res.status(500).json({
				message : "Database Error"
			});
		} else{
			if(user){
				if (req.body.name)
					user.name = req.body.name
				if (req.body.dept)
					user.dept = req.body.dept

				user.save(function(err){
					if(err){
						res.status(500).json({
							message : "Database Error"
						});
					} else{
						res.json({
							message : "User Successfully Updated!"
						});
					}
				});
			} else{
				res.json({
					message : "User Doesn't Exists!"
				});
			}
		}
	}) ;
})


// Delete a User
router.delete('/:sid', function(req,res,next){
	sid = req.params.sid ;

	User.findOne({sid : sid}, function(error,user){
		if (error){
			res.status(500).json({
				message : "Database Error"
			});
		} else{
			if (user){
				user.remove({sid:sid},function(error,user){
					if (error){
						res.status(500).json({
							message : "Database Error"
						});
					} else{
						res.status(500).json({
							message : "User Successfully Deleted!"
						});
					}
				});
			} else{
				res.status(500).json({
					message : "User Doesn't Exists!"
				});
			}
		}
	});
});


module.exports = router;